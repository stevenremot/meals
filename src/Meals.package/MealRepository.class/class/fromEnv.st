instance creation
fromEnv
	| env |
	env := OSEnvironment default.
	((env getEnv: 'DB_TYPE') = 'mongo')
		ifTrue: [ SCRAMSHA1AuthMechanism beDefault.
			^ self
				mongoHost: (env getEnv: 'DB_HOST')
				database: (env getEnv: 'DB_NAME')
				user: (env getEnv: 'DB_USER')
				password: (env getEnv: 'DB_PASSWORD') ]
		ifFalse: [ ^ self memory ]