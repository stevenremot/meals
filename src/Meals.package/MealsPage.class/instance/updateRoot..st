updating
updateRoot: anHtmlRoot
	super updateRoot: anHtmlRoot.
	anHtmlRoot title: self title.
	anHtmlRoot stylesheet
		url:
			'https://fonts.googleapis.com/icon?family=Material+Icons'.
			
	anHtmlRoot stylesheet
		url:
			'https://code.getmdl.io/1.3.0/material.indigo-pink.min.css'.
	anHtmlRoot script defer
		url:
			'https://code.getmdl.io/1.3.0/material.min.js'.
	anHtmlRoot meta
		name: 'viewport';
		content: 'width=device-width'