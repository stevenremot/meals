rendering
style 
	^ 'form {
  display: flex;
  flex-direction: column;
  padding: 12px;
  align-items: stretch;
}

form > * {
  margin-bottom: 12px;
}'