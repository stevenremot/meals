rendering
renderBodyOn: html
	html
		form: [ html div
				class: 'mdl-textfield mdl-js-textfield mdl-textfield--floating-label';
				with: [ html textInput
						id: 'mealName';
						class: 'mdl-textfield__input';
						on: #mealName of: model.
					html label
						class: 'mdl-textfield__label';
						for: 'mealName';
						with: 'Name' ].
			html select
				list: #(sage pasSage);
				selected: model tags first;
				callback: [ :value | model tags: {value} ].
			html submitButton
				class: 'mdl-button mdl-js-button mdl-button--raised mdl-button--colored';
				on: #save of: self;
				with: 'Save' ]