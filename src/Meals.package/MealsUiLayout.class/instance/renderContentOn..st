rendering
renderContentOn: html
	html div
		class:
			'mdl-layout mdl-js-layout mdl-layout--fixed-header';
		with: [ html header
				class: 'mdl-layout__header';
				with: [ html div
						class: 'mdl-layout__header-row';
						with: [ html span
								class: 'mdl-layout-title';
								with: title ].
					tabBar
						ifNotNil: [ html render: tabBar ] ].
			html div
				class: 'mdl-layout__content';
				with: content ]