rendering
renderTab: tab on: html
	html anchor
		callback: [ (tab at: #action) value ];
		class:
			((tab at: #value) = selectedTab
				ifTrue: [ 'mdl-layout__tab is-active' ]
				ifFalse: [ 'mdl-layout__tab' ]);
		attributeAt: 'data-tab'
			put: (tab at: #value);
		with: (tab at: #label)