rendering
renderContentOn: html
	html div
		class: 'mdl-layout__tab-bar mdl-js-ripple-effect';
		with: [ tabs
				do:
					[ :tab | self renderTab: tab on: html ] ]