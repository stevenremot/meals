accessing
tabs
	^ tabs
		ifNil: [ tabs := MealsUiTabBar new
				tabs: self tabSpec;
				selectedTab: #sage;
				yourself ]