accessing
tabSpec
	^ {{(#label -> 'Sage').
	(#value -> #sage).
	(#action -> [ self selectedTab: #sage ])}
		asDictionary.
	{(#label -> 'Pas sage').
	(#value -> #pasSage).
	(#action -> [ self selectedTab: #pasSage ])}
		asDictionary}