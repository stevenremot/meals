rendering
renderBodyOn: html
	html unorderedList
		class: 'mdl-list js-list';
		with: [ (self repository mealsForTag: self selectedTab)
				do: [ :meal | 
					html listItem
						class: 'mdl-list__item';
						with: [ html div
								class: 'mdl-list__item-primary-content';
								with: meal mealName.
							html anchor
								class: 'mdl-list__item-secondary-action';
								callback: [ self remove: meal ];
								with: [ html emphasis
										class: 'material-icons';
										with: 'delete' ] ] ] ].
	html anchor
		class: 'mdl-button mdl-js-button mdl-button--fab mdl-button--colored floating-button';
		callback: [ self call: MealsAddMealPage new ];
		with: [ html emphasis
				class: 'material-icons';
				with: 'add' ].
	(html tag: 'template')
		id: 'list-template';
		with: [ html listItem
				class: 'mdl-list__item js-content';
				with: '' ]