baselines
baseline01: spec
	<version: '0.1-baseline'>
	spec
		for: #common
		do: [ spec blessing: #baseline.
			spec package: 'Meals' with: [ spec requires: #('Seaside3' 'VoyageMongo') ].
			spec repository: 'bitbucket://stevenremot/meals/src'.
			
			spec
				project: 'Seaside3'
				with: [ spec
						className: 'ConfigurationOfSeaside3';
						version: #stable;
						repository: 'http://www.smalltalkhub.com/mc/Seaside/MetacelloConfigurations/main' ].
			spec
				project: 'VoyageMongo'
				with: [ spec
						className: 'ConfigurationOfVoyageMongo';
						version: #stable;
						repository: 'github://pharo-nosql/voyage/mc' ] ]