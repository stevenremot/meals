#!/bin/bash
set -e

mongo <<EOF
use admin
db.createUser({
  user:  '$DB_USER',
  pwd: '$DB_PASSWORD',
  roles: [{
    role: 'readWrite',
    db: 'meals'
  }]
})
EOF
