WAAdmin disableDevelopmentTools;
	 unregister: 'tools';
	 unregister: 'examples';
	 unregister: 'seaside';
	 unregister: 'welcome';
	 unregister: 'status';
	 unregister: 'tests';
	 unregister: 'browse';
	unregister: 'config'.

(ZnZincServerAdaptor port: 8080) start.
ZnZincServerAdaptor default server debugMode: false.
ZnZincServerAdaptor default server logToTranscript.

(WAAdmin register: MealsApp asApplicationAt: 'meals')
	 preferenceAt: #serverPath put: '/'.
WAAdmin defaultDispatcher defaultName: 'meals'.