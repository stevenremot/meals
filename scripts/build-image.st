Gofer
	new
		repository:
			(MCBitbucketRepository
				location: 'bitbucket://stevenremot/meals/src');
		package: 'ConfigurationOfMeals';
		load.

(Smalltalk at: #ConfigurationOfMeals) loadDevelopment.

ImageCleaner cleanUpForProduction.

SmalltalkImage current saveAs: '/var/local/build/Meals'.