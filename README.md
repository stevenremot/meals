# Meals

Simple app for registering meals ideas.

## Setup

From a fresh Pharo 6 image, run:

```st
Gofer
	new
		repository:
			(MCBitbucketRepository
				location: 'bitbucket://stevenremot/meals/src');
		package: 'ConfigurationOfMeals';
		load.

(Smalltalk at: #ConfigurationOfMeals) loadDevelopment.
```

## Run server for development

Start a seaside server with the app accessible at
"http://localhost:8088/meals"

```st
(ZnZincServerAdaptor port: 8088) start.
WAAdmin register: MealsApp asApplicationAt: '/meals'.
```

## Run release image locally

```sh
# Create the docker image for building the pharo image
make docker.build

# Build the image and start the server on port 8082
make start.test
```

## Deploy app

```sh
make clean
make image

REMOTE_HOST=<HOST> \
REMOTE_DEST=<REMOTE_DIRECTORY> \
PORT=<PORT> \
DB_PASSWORD=<DB_PASSWORD> \
make deploy
```
