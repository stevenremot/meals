.PHONY: image start clean logs

$PWD=`pwd`

docker.build:
	docker build -f docker/Dockerfile.build -t meals-build --build-arg DOCKER_USER=$USER .

build/Meals.image:
	@mkdir -p build
	docker run -v ${PWD}/build/:/var/local/build/ meals-build eval `cat scripts/build-image.st`

image: build/Meals.image

start: image
	DB_USER=meals DB_PASSWORD=$(DB_PASSWORD) PORT=$(PORT) docker-compose -f docker-compose.prod.yml up --build -d

start.test:
	DB_PASSWORD=password PORT=8082 $(MAKE) start

stop:
	PORT=8000 docker-compose -f docker-compose.prod.yml down

logs:
	PORT=8000 docker-compose -f docker-compose.prod.yml logs -f

clean:
	echo "Requires sudo because the docker.build image has root rights"
	sudo rm -rf build

deploy: image
	rsync -rR Makefile docker-compose.prod.yml build/ deploy/ ${REMOTE_HOST}:${REMOTE_DEST}
	ssh ${REMOTE_HOST} "cd ${REMOTE_DEST}; make stop; PORT=${PORT} DB_PASSWORD=${DB_PASSWORD} make start"
